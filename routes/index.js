const express = require('express');
const process  = require('../lib/process-image');
const router = express.Router();

// default route
router.get('/', (req, res, next) => {
  res.json({message: ':('});
});

/**
 * Core route
 *
 * @param <Integer> width, expected image width
 * @param <Integer> height, expected image height
 * @param <Integer> quality, expected image quality(%), 60% default
 * @param <Integer> percent, if resize is by percent, expected image percent
 * @param <Integer> color, expected image width
 * @param <String>  url, expected image width
 * @callback <BUFFER>, an image buffer
 */
router.use('/w/:width/h/:height/q/:quality/percent/:percent/c/:color/url/*', (req, res, next) => {

    const params = {
        url: req.params['0'],
        host: req.get('host'),
        percent: req.params.percent,
        width: req.params.width,
        height: req.params.height,
        quality: req.params.quality || 60,
        color: req.params.color
    };

    process(params, (buffer) => {
        res.end(buffer);
    });

})

module.exports = router;

const gm = require('gm');
const path = require('path');
const imagic = gm.subClass({imageMagick: true});
const request = require('request');

const imageDir = path.join(__dirname, '../public/images/');
const defaultImage = `${imageDir}default.png`;

// Public access
exports = module.exports = (paramsIn, callback) => {

  const params = processParams(paramsIn);

  imagic(request(params.url))
    .compress('JPEG')
    .resize(params.resize.width, params.resize.height, params.resize.type)
    .gravity('Center')
    .type(params.color)
    .crop(params.resize.cropWidth, params.resize.cropHeight)
    .quality(paramsIn.quality)
    .toBuffer((error, buffer) => {
      if(error) {
        imagic(defaultImage)
        .toBuffer((error, bufferDefault) => {
          callback(bufferDefault);
        });
      }else {
        callback(buffer);
      }
    })

}

/**
 * Private helper function
 *
 * @param <Object> params, initial params
 * @return <Object>, params with some changes
 */
const processParams = (params) => {

  // check localhost enviroment
  const runningLocalhost = (params.host.indexOf('localhost') !== -1);

  // check options values based on type resize
  const resize = (!parseInt(params.percent))
             ? {
                 width: params.width,
                 height: params.height,
                 type: '^' ,
                 cropWidth: params.width,
                 cropHeight: params.height
             }
             : {
                 width: params.percent,
                 height: null,
                 type: '%' ,
                 cropWidth: -1,
                 cropHeight: -1
             }

 const color = (!parseInt(params.color))
           ? 'Grayscale'
           : 'TrueColor'

 var url = (runningLocalhost)
      ? `http://${params.url}`
      : `//${params.url}`;

// remove http or https from url
url =  url.replace(/^https?\:\/\//i, "");

  return {
    url: url,
    color: color,
    resize: resize
  }

}

# Image Processor
### On the fly image processing

## Dependencies
  > GraphicsMagic:
    Installation guide: https://gist.github.com/abernardobr/f7b3190176b90f2ac4b2

## Install
> npm i

## Run
> node app.js

## Example use

> http://localhost:3000/w/320/h/140/q/60/percent/0/c/1/url/https://cdn.pixabay.com/photo/2017/01/11/18/31/toucan-1972560_960_720.jpg

### Params
- w: width, integer, number in pixels, 320 means 320px
- h: height, interger too, number in pixels, 140 means 140px
- q: quality, integer, represent the image quality from 1 to 100 %, 60 means 60% image quality
- p: percent, integer, represent image %, p > 0 means we want image in %, 0 in pixels
- c: color, some value different to 0, means that we want the image with color, in other case our service apply grayscale filter
- url: original image url
